import unittest
from linked_list import Node
from linked_list import LinkedList

class TestLinkedList(unittest.TestCase):

    def setUp(self):
        self.new_list = LinkedList()

    def tearDown(self):
        pass

    def test_prepend(self):
        # Test for adding intergers to list
        self.new_list.prepend(1)
        self.assertEqual(self.new_list.head.data, 1)

        # Test for adding strings
        self.new_list.prepend("A simple node")
        self.assertEqual(self.new_list.head.data, "A simple node")

        # Test for adding array
        self.new_list.prepend(["this", "is", "an", "array", "with", "int", "five", 5])
        self.assertEqual(self.new_list.head.data,(["this", "is", "an", "array", "with", "int", "five", 5]))

        # I should also test for not giving an input, and giving two inputs
    def test_get_size(self):
        # Test that size of empty list is 0
        self.assertEqual(self.new_list.get_size(), 0)

        # Test for correct size of list
        for x in range(0,10):
            self.new_list.prepend(x)
        self.assertEqual(self.new_list.get_size(), 10)

    def test_pop(self):
        # Test for empty linked list
        self.assertEqual(self.new_list.pop(), None)

        # Test for popping interger
        self.new_list.prepend(7)
        self.assertEqual(self.new_list.pop(), 7)

        # Test for popping string
        self.new_list.prepend("String")
        self.assertEqual(self.new_list.pop(), "String")


    def test_append_item(self):
        # Test that appending item to empty list creates a new head node
        self.assertEqual(self.new_list.append_item(10), True)
        self.assertEqual(self.new_list.head.data, 10)

        # Test for appending a string and an array
        self.assertEqual(self.new_list.append_item("A String"), True)
        self.assertEqual(self.new_list.append_item(["An", "Array", "of", "Strings"]), True)



    def test_remove(self):
        #Test for empty list
        self.assertEqual(self.new_list.remove(9), False)

        #Test for removing single item
        self.new_list.prepend("to be removed")
        self.assertEqual(self.new_list.remove("to remove"), False)
        self.assertEqual(self.new_list.remove("to be removed"), True)

    def test_find(self):
        # Finding item in list should return True
        self.new_list.prepend("item")
        self.assertEqual(self.new_list.find("item"), True)
        self.assertEqual(self.new_list.find("var"), False)


    def test_list_data(self):
        # Printing empty list should return empty array
        self.assertEqual(self.new_list.list_data(), [])

        arr = ["Seven", 1, 5, "Nine", "Eight", 100, 2]
        for x in arr:
            self.new_list.prepend(x)
        self.assertEqual(self.new_list.list_data(), arr[::-1])


if __name__ == '__main__':
    unittest.main()
