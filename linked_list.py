class Node(object):
    ## Creating the node object where data is the value of the node
    def __init__ (self, data, node = None):
        self.data = data
        self.next_node = node
    def get_data(self):
        return self.data
    def set_data(self, data):
        self.data = data
    def get_next(self):
        return self.next_node
    def set_next(self, node):
        self.next_node = node

class LinkedList(object):
    # Initialize list
    def __init__ (self, head = None):
        self.head = head
        self.size = 0

    # Get size of list
    def get_size (self):
        return self.size
        # print("The size of this linked list is: {}".format(self.size))

    # Add node to beginning of list
    def prepend (self, data):
        new_node = Node(data, self.head)
        self.head = new_node
        self.size += 1

    # Here I'm using a try/except blocks to handle the exception of having this_node = None when
    # the linked list is empty.
    def pop(self):
        this_node = self.head
        try:
            popped_item = this_node
            self.head = this_node.get_next()
            self.size -= 1
            return popped_item.data
        except AttributeError:
            print("AttributeError: Linked List should have at least one item before using pop().")

    ## Append: In this approach I used the logic of the remove_last_item method to
    ## go down to the last item in the list, then create a new node
    def append_item (self, data):
        this_node = self.head
        next_node = None
        new_node = Node(data)
        while this_node:
            if this_node.get_next():
                this_node = this_node.get_next()
            else:
                this_node.set_next(new_node)
                self.size += 1
                return True
        else:
            self.prepend(data)
            return True

    ## RemoveNode: remove node from list
    ## We want to start at the head then go down the chain until the current
    ## node's data is equal to the input
    def remove (self, data):
        this_node = self.head
        prev_node = None
        while this_node:
            if this_node.get_data() == data:
                if prev_node:
                    prev_node.set_next(this_node.get_next())
                else:
                    self.head = this_node.get_next()
                self.size -= 1
                return True
            else:
                prev_node = this_node
                this_node = this_node.get_next()
        return False

    # Find: Check to see if data is contained in Linked List
    # Similar to Remove, we want to start at head, then go down to chain,
    # checking if each node's data equals the user input.
    # If we reach end of list with no success, return False.
    def find (self, data):
        this_node = self.head
        while this_node:
            if this_node.get_data() == data:
                return True
            else:
                this_node = this_node.get_next()
        return False

    # list_data: iterate through list and print data of each node
    def list_data (self):
        this_node = self.head
        arr = []
        while this_node:
            arr.append(this_node.get_data())
            this_node = this_node.get_next()
        return arr
